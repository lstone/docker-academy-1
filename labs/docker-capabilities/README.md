
# Docker capabilities lab

In this lab we play around with the linux capabilities inside docker 
containers. The man page `man capabilities` contains all capabilities 
provided by the Linux kernel.

Docker enables only a subset of the available capabilities. The default 
capabilities can be found in the following source file: 
[default_template_linux.go](https://github.com/docker/docker/blob/master/daemon/execdriver/native/template/default_template_linux.go) and check out the [additional capabilities](https://github.com/docker/libcontainer/blob/master/capabilities_linux.go)

With the docker command line arguments `--cap-drop` and `--cap-add` we 
can control the capabilities used for a specific container.

## Example:

```sh
docker run -it --rm --cap-drop NET_BIND_SERVICE busybox:1.24.1
/ # nc -l -p 80
nc: bind: Permission denied
```

## Task 1

Find away to set the system time inside a Docker container with the 
command ``date -s 09:41```. Use the same Docker image as shown in the 
example.